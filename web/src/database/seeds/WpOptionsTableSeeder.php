<?php

use Illuminate\Database\Seeder;

class WpOptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wp_options')->delete();
        
        \DB::table('wp_options')->insert(array (
            0 => 
            array (
                'option_id' => 1,
                'option_name' => 'siteurl',
                'option_value' => 'http://localhost:8000/wordpress',
                'autoload' => 'yes',
            ),
            1 => 
            array (
                'option_id' => 2,
                'option_name' => 'home',
                'option_value' => 'http://localhost:8000/wordpress',
                'autoload' => 'yes',
            ),
            2 => 
            array (
                'option_id' => 3,
                'option_name' => 'blogname',
                'option_value' => 'ADK WP Laravel',
                'autoload' => 'yes',
            ),
            3 => 
            array (
                'option_id' => 4,
                'option_name' => 'blogdescription',
                'option_value' => 'Just another WordPress site',
                'autoload' => 'yes',
            ),
            4 => 
            array (
                'option_id' => 5,
                'option_name' => 'users_can_register',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            5 => 
            array (
                'option_id' => 6,
                'option_name' => 'admin_email',
                'option_value' => 'yahyazini@gmail.com',
                'autoload' => 'yes',
            ),
            6 => 
            array (
                'option_id' => 7,
                'option_name' => 'start_of_week',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            7 => 
            array (
                'option_id' => 8,
                'option_name' => 'use_balanceTags',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            8 => 
            array (
                'option_id' => 9,
                'option_name' => 'use_smilies',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            9 => 
            array (
                'option_id' => 10,
                'option_name' => 'require_name_email',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            10 => 
            array (
                'option_id' => 11,
                'option_name' => 'comments_notify',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            11 => 
            array (
                'option_id' => 12,
                'option_name' => 'posts_per_rss',
                'option_value' => '10',
                'autoload' => 'yes',
            ),
            12 => 
            array (
                'option_id' => 13,
                'option_name' => 'rss_use_excerpt',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            13 => 
            array (
                'option_id' => 14,
                'option_name' => 'mailserver_url',
                'option_value' => 'mail.example.com',
                'autoload' => 'yes',
            ),
            14 => 
            array (
                'option_id' => 15,
                'option_name' => 'mailserver_login',
                'option_value' => 'login@example.com',
                'autoload' => 'yes',
            ),
            15 => 
            array (
                'option_id' => 16,
                'option_name' => 'mailserver_pass',
                'option_value' => 'password',
                'autoload' => 'yes',
            ),
            16 => 
            array (
                'option_id' => 17,
                'option_name' => 'mailserver_port',
                'option_value' => '110',
                'autoload' => 'yes',
            ),
            17 => 
            array (
                'option_id' => 18,
                'option_name' => 'default_category',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            18 => 
            array (
                'option_id' => 19,
                'option_name' => 'default_comment_status',
                'option_value' => 'open',
                'autoload' => 'yes',
            ),
            19 => 
            array (
                'option_id' => 20,
                'option_name' => 'default_ping_status',
                'option_value' => 'open',
                'autoload' => 'yes',
            ),
            20 => 
            array (
                'option_id' => 21,
                'option_name' => 'default_pingback_flag',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            21 => 
            array (
                'option_id' => 22,
                'option_name' => 'posts_per_page',
                'option_value' => '10',
                'autoload' => 'yes',
            ),
            22 => 
            array (
                'option_id' => 23,
                'option_name' => 'date_format',
                'option_value' => 'F j, Y',
                'autoload' => 'yes',
            ),
            23 => 
            array (
                'option_id' => 24,
                'option_name' => 'time_format',
                'option_value' => 'g:i a',
                'autoload' => 'yes',
            ),
            24 => 
            array (
                'option_id' => 25,
                'option_name' => 'links_updated_date_format',
                'option_value' => 'F j, Y g:i a',
                'autoload' => 'yes',
            ),
            25 => 
            array (
                'option_id' => 26,
                'option_name' => 'comment_moderation',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            26 => 
            array (
                'option_id' => 27,
                'option_name' => 'moderation_notify',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            27 => 
            array (
                'option_id' => 28,
                'option_name' => 'permalink_structure',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            28 => 
            array (
                'option_id' => 29,
                'option_name' => 'rewrite_rules',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            29 => 
            array (
                'option_id' => 30,
                'option_name' => 'hack_file',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            30 => 
            array (
                'option_id' => 31,
                'option_name' => 'blog_charset',
                'option_value' => 'UTF-8',
                'autoload' => 'yes',
            ),
            31 => 
            array (
                'option_id' => 32,
                'option_name' => 'moderation_keys',
                'option_value' => '',
                'autoload' => 'no',
            ),
            32 => 
            array (
                'option_id' => 33,
                'option_name' => 'active_plugins',
                'option_value' => 'a:0:{}',
                'autoload' => 'yes',
            ),
            33 => 
            array (
                'option_id' => 34,
                'option_name' => 'category_base',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            34 => 
            array (
                'option_id' => 35,
                'option_name' => 'ping_sites',
                'option_value' => 'http://rpc.pingomatic.com/',
                'autoload' => 'yes',
            ),
            35 => 
            array (
                'option_id' => 36,
                'option_name' => 'comment_max_links',
                'option_value' => '2',
                'autoload' => 'yes',
            ),
            36 => 
            array (
                'option_id' => 37,
                'option_name' => 'gmt_offset',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            37 => 
            array (
                'option_id' => 38,
                'option_name' => 'default_email_category',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            38 => 
            array (
                'option_id' => 39,
                'option_name' => 'recently_edited',
                'option_value' => '',
                'autoload' => 'no',
            ),
            39 => 
            array (
                'option_id' => 40,
                'option_name' => 'template',
                'option_value' => 'twentyseventeen',
                'autoload' => 'yes',
            ),
            40 => 
            array (
                'option_id' => 41,
                'option_name' => 'stylesheet',
                'option_value' => 'twentyseventeen',
                'autoload' => 'yes',
            ),
            41 => 
            array (
                'option_id' => 42,
                'option_name' => 'comment_whitelist',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            42 => 
            array (
                'option_id' => 43,
                'option_name' => 'blacklist_keys',
                'option_value' => '',
                'autoload' => 'no',
            ),
            43 => 
            array (
                'option_id' => 44,
                'option_name' => 'comment_registration',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            44 => 
            array (
                'option_id' => 45,
                'option_name' => 'html_type',
                'option_value' => 'text/html',
                'autoload' => 'yes',
            ),
            45 => 
            array (
                'option_id' => 46,
                'option_name' => 'use_trackback',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            46 => 
            array (
                'option_id' => 47,
                'option_name' => 'default_role',
                'option_value' => 'subscriber',
                'autoload' => 'yes',
            ),
            47 => 
            array (
                'option_id' => 48,
                'option_name' => 'db_version',
                'option_value' => '38590',
                'autoload' => 'yes',
            ),
            48 => 
            array (
                'option_id' => 49,
                'option_name' => 'uploads_use_yearmonth_folders',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            49 => 
            array (
                'option_id' => 50,
                'option_name' => 'upload_path',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            50 => 
            array (
                'option_id' => 51,
                'option_name' => 'blog_public',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            51 => 
            array (
                'option_id' => 52,
                'option_name' => 'default_link_category',
                'option_value' => '2',
                'autoload' => 'yes',
            ),
            52 => 
            array (
                'option_id' => 53,
                'option_name' => 'show_on_front',
                'option_value' => 'posts',
                'autoload' => 'yes',
            ),
            53 => 
            array (
                'option_id' => 54,
                'option_name' => 'tag_base',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            54 => 
            array (
                'option_id' => 55,
                'option_name' => 'show_avatars',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            55 => 
            array (
                'option_id' => 56,
                'option_name' => 'avatar_rating',
                'option_value' => 'G',
                'autoload' => 'yes',
            ),
            56 => 
            array (
                'option_id' => 57,
                'option_name' => 'upload_url_path',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            57 => 
            array (
                'option_id' => 58,
                'option_name' => 'thumbnail_size_w',
                'option_value' => '150',
                'autoload' => 'yes',
            ),
            58 => 
            array (
                'option_id' => 59,
                'option_name' => 'thumbnail_size_h',
                'option_value' => '150',
                'autoload' => 'yes',
            ),
            59 => 
            array (
                'option_id' => 60,
                'option_name' => 'thumbnail_crop',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            60 => 
            array (
                'option_id' => 61,
                'option_name' => 'medium_size_w',
                'option_value' => '300',
                'autoload' => 'yes',
            ),
            61 => 
            array (
                'option_id' => 62,
                'option_name' => 'medium_size_h',
                'option_value' => '300',
                'autoload' => 'yes',
            ),
            62 => 
            array (
                'option_id' => 63,
                'option_name' => 'avatar_default',
                'option_value' => 'mystery',
                'autoload' => 'yes',
            ),
            63 => 
            array (
                'option_id' => 64,
                'option_name' => 'large_size_w',
                'option_value' => '1024',
                'autoload' => 'yes',
            ),
            64 => 
            array (
                'option_id' => 65,
                'option_name' => 'large_size_h',
                'option_value' => '1024',
                'autoload' => 'yes',
            ),
            65 => 
            array (
                'option_id' => 66,
                'option_name' => 'image_default_link_type',
                'option_value' => 'none',
                'autoload' => 'yes',
            ),
            66 => 
            array (
                'option_id' => 67,
                'option_name' => 'image_default_size',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            67 => 
            array (
                'option_id' => 68,
                'option_name' => 'image_default_align',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            68 => 
            array (
                'option_id' => 69,
                'option_name' => 'close_comments_for_old_posts',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            69 => 
            array (
                'option_id' => 70,
                'option_name' => 'close_comments_days_old',
                'option_value' => '14',
                'autoload' => 'yes',
            ),
            70 => 
            array (
                'option_id' => 71,
                'option_name' => 'thread_comments',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            71 => 
            array (
                'option_id' => 72,
                'option_name' => 'thread_comments_depth',
                'option_value' => '5',
                'autoload' => 'yes',
            ),
            72 => 
            array (
                'option_id' => 73,
                'option_name' => 'page_comments',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            73 => 
            array (
                'option_id' => 74,
                'option_name' => 'comments_per_page',
                'option_value' => '50',
                'autoload' => 'yes',
            ),
            74 => 
            array (
                'option_id' => 75,
                'option_name' => 'default_comments_page',
                'option_value' => 'newest',
                'autoload' => 'yes',
            ),
            75 => 
            array (
                'option_id' => 76,
                'option_name' => 'comment_order',
                'option_value' => 'asc',
                'autoload' => 'yes',
            ),
            76 => 
            array (
                'option_id' => 77,
                'option_name' => 'sticky_posts',
                'option_value' => 'a:0:{}',
                'autoload' => 'yes',
            ),
            77 => 
            array (
                'option_id' => 78,
                'option_name' => 'widget_categories',
                'option_value' => 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            78 => 
            array (
                'option_id' => 79,
                'option_name' => 'widget_text',
                'option_value' => 'a:0:{}',
                'autoload' => 'yes',
            ),
            79 => 
            array (
                'option_id' => 80,
                'option_name' => 'widget_rss',
                'option_value' => 'a:0:{}',
                'autoload' => 'yes',
            ),
            80 => 
            array (
                'option_id' => 81,
                'option_name' => 'uninstall_plugins',
                'option_value' => 'a:0:{}',
                'autoload' => 'no',
            ),
            81 => 
            array (
                'option_id' => 82,
                'option_name' => 'timezone_string',
                'option_value' => '',
                'autoload' => 'yes',
            ),
            82 => 
            array (
                'option_id' => 83,
                'option_name' => 'page_for_posts',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            83 => 
            array (
                'option_id' => 84,
                'option_name' => 'page_on_front',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            84 => 
            array (
                'option_id' => 85,
                'option_name' => 'default_post_format',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            85 => 
            array (
                'option_id' => 86,
                'option_name' => 'link_manager_enabled',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            86 => 
            array (
                'option_id' => 87,
                'option_name' => 'finished_splitting_shared_terms',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            87 => 
            array (
                'option_id' => 88,
                'option_name' => 'site_icon',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            88 => 
            array (
                'option_id' => 89,
                'option_name' => 'medium_large_size_w',
                'option_value' => '768',
                'autoload' => 'yes',
            ),
            89 => 
            array (
                'option_id' => 90,
                'option_name' => 'medium_large_size_h',
                'option_value' => '0',
                'autoload' => 'yes',
            ),
            90 => 
            array (
                'option_id' => 91,
                'option_name' => 'initial_db_version',
                'option_value' => '38590',
                'autoload' => 'yes',
            ),
            91 => 
            array (
                'option_id' => 92,
                'option_name' => 'wp_user_roles',
                'option_value' => 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}',
                'autoload' => 'yes',
            ),
            92 => 
            array (
                'option_id' => 93,
                'option_name' => 'fresh_site',
                'option_value' => '1',
                'autoload' => 'yes',
            ),
            93 => 
            array (
                'option_id' => 94,
                'option_name' => 'widget_search',
                'option_value' => 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            94 => 
            array (
                'option_id' => 95,
                'option_name' => 'widget_recent-posts',
                'option_value' => 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            95 => 
            array (
                'option_id' => 96,
                'option_name' => 'widget_recent-comments',
                'option_value' => 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            96 => 
            array (
                'option_id' => 97,
                'option_name' => 'widget_archives',
                'option_value' => 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            97 => 
            array (
                'option_id' => 98,
                'option_name' => 'widget_meta',
                'option_value' => 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}',
                'autoload' => 'yes',
            ),
            98 => 
            array (
                'option_id' => 99,
                'option_name' => 'sidebars_widgets',
                'option_value' => 'a:5:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}s:13:"array_version";i:3;}',
                'autoload' => 'yes',
            ),
        ));
        
        
    }
}