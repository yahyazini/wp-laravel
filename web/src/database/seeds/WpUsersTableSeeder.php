<?php

use Illuminate\Database\Seeder;

class WpUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wp_users')->delete();
        
        \DB::table('wp_users')->insert(array (
            0 => 
            array (
                'ID' => 1,
                'user_login' => 'adkgroup',
                'user_pass' => '$P$BKzPfQoDGlsv2cEV7rTLwJpBOh2Dv21',
                'user_nicename' => 'adkgroup',
                'user_email' => 'yahyazini@gmail.com',
                'user_url' => '',
                'user_registered' => '2017-08-08 15:49:45',
                'user_activation_key' => '',
                'user_status' => 0,
                'display_name' => 'adkgroup',
            ),
        ));
        
        
    }
}