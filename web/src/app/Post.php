<?php 

namespace App;

use Corcel\Model\Post as Corcel;

class Post extends Corcel
{
    protected $fillable = ['post_name', 'post_content'];
    // protected $guarded = [];
}