<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Post as Post;

class PostsController extends Controller
{
    public function index()
    {   
        $posts = Post::published()->get();
        return view('posts.index')->with('publications', $posts);
    }
    
    public function published()
    {
        $p = Post::published()->get();
        return $p;
        // return response()->json($p, 200);
    }
}
